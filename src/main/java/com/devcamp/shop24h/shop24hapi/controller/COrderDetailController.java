package com.devcamp.shop24h.shop24hapi.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.COrderDetails;
import com.devcamp.shop24h.shop24hapi.service.COrderDetailService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class COrderDetailController {
    @Autowired
    private COrderDetailService orderDetailService;   
    
    // ================== PUBLIC ===================== 

    // Tạo chi tiết đơn hàng( 1 hoặc nhiều sản phẩm) với id của đơn hàng đã tạo

    @PostMapping("api/noauth/order-details")    
    public ResponseEntity<?> createOrderDetails(@RequestParam Integer orderId,
    @RequestParam Integer productId,@RequestBody COrderDetails orderDetails){
        return orderDetailService.createOrderDetails(orderId, productId, orderDetails);
    }

    // ================== MANAGER AND CUSTOMER ==================
    // Lấy chi tiết đơn hàng qua order Id
    @GetMapping("order-details/orders/{orderId}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<?> getOrderDetailsByOrderId(HttpServletRequest request,@PathVariable Integer orderId){
        return orderDetailService.getOrderDetailsByOrderId(request, orderId);
    }
    //====================MANAGER======================
    // Lấy chi tiết đơn hàng qua order Id cho admin
    @GetMapping("order-details/orders/admin/{orderId}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<?> getOrderDetailsByOrderId(@PathVariable Integer orderId){
        return orderDetailService.getOrderDetailsByOrderId(orderId);
    }
    // xoá chi tiết đơn hàng qua order Id cho admin
    @DeleteMapping("order-details/{orderDetailsId}/product/{productId}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<?> deleteOrderDetailsByOrderDetailsId(@PathVariable Integer orderDetailsId,@PathVariable Integer productId){
        return  orderDetailService.deleteOrderDetailsByOrderDetailsId(orderDetailsId,productId);
    }
    // cập nhật chi tiết đơn hàng qua order Id cho admin
    @PutMapping("order-details/{orderDetailsId}/product/{productId}")
    @PreAuthorize("hasRole('MANAGER')")
     public ResponseEntity<?> updateOrderDetailsById(@PathVariable Integer orderDetailsId,@PathVariable Integer productId,@RequestBody COrderDetails orderDetails){
        return orderDetailService.updateOrderDetailsById(orderDetailsId,productId, orderDetails);
     }
}

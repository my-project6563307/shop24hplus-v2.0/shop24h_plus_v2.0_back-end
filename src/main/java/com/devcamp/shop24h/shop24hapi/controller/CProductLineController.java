package com.devcamp.shop24h.shop24hapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.CProductLines;
import com.devcamp.shop24h.shop24hapi.service.CProductLineService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
@PreAuthorize("hasRole('MANAGER')")
public class CProductLineController {
    @Autowired
    private CProductLineService productLineService;

    // ================ MỤC DÀNH CHO QUẢN TRỊ VIÊN(MANAGER) ===================

    // Lấy danh sách dòng sản phẩm
    @GetMapping("product-lines")
    public ResponseEntity<List<CProductLines>> getAllProductLines(@RequestParam(defaultValue = "0") String page,
    @RequestParam(defaultValue = "10") String size){
        return productLineService.getAllProductLines(page, size);
    }
    // Lấy dòng sản phẩm qua id
    @GetMapping("product-lines/{id}")
    public ResponseEntity<CProductLines> getProductLineById (@PathVariable Integer id){
        return productLineService.getProductLineById(id);
    }

    // Cập nhật các dòng sản phẩm qua id
    @PutMapping("product-lines/{id}")
    public ResponseEntity<Object> updateProductLineById(@PathVariable Integer id,@RequestBody CProductLines productLine){
        return productLineService.updateProductLineById(id, productLine);
    }

    // Thêm mới dòng sản phẩm
    @PostMapping("product-lines")
    public ResponseEntity<Object> createProductLine(@RequestBody CProductLines productLine){
        return productLineService.createProductLine(productLine);
    }

     // Xoá thông tin 1 dòng sản phẩm qua id
     @DeleteMapping("product-lines/{id}")
     public ResponseEntity<Object> deleteProductLineById(@PathVariable Integer id){
        return productLineService.deleteProductLineById(id);
     }
}

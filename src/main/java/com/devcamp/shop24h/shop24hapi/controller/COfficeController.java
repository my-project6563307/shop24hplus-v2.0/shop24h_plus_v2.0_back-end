package com.devcamp.shop24h.shop24hapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.COffices;
import com.devcamp.shop24h.shop24hapi.service.COfficeService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class COfficeController {
    @Autowired
    private COfficeService officeService;

    // ================ MỤC DÀNH CHO QUẢN TRỊ VIÊN (MANAGER ROLE)
    // =====================

    // lấy toàn bộ danh sách văn phòng của shop24h
    @GetMapping("api/noauth/offices")
    public ResponseEntity<List<COffices>> getAllOffices(@RequestParam(defaultValue = "0") String page,
            @RequestParam(defaultValue = "20") String size) {
        return officeService.getAllOffices(page, size);
    }

    // Lấy thông tin văn phòng qua id văn phòng
    @GetMapping("offices/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<COffices> getOfficeById(@PathVariable Integer id) {
        return officeService.getOfficeById(id);
    }

    // Cập nhật thông tin văn phòng qua id
    @PutMapping("offices/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> updateOfficeById(@PathVariable Integer id, @RequestBody COffices pOffice) {
        return officeService.updateOfficeById(id, pOffice);
    }

    // Thêm mới văn phòng
    @PostMapping("offices")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> createOffice(@RequestBody COffices pOffice) {
        return officeService.createOffice(pOffice);
    }

    // Xoá thông tin 1 văn phòng
    @DeleteMapping("offices/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> deleteOfficeById(@PathVariable Integer id) {
        return officeService.deleteOfficeById(id);
    }
}

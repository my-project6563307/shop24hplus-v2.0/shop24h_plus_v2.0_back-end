package com.devcamp.shop24h.shop24hapi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.COrders;
import com.devcamp.shop24h.shop24hapi.repository.IOrdersRepository;
import com.devcamp.shop24h.shop24hapi.service.COrderService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class COrderController {
    @Autowired
    private COrderService orderService;
    @Autowired
    private IOrdersRepository ordersRepository;

    // ============ PUBLIC ==============================

    // Tạo order qua id khách hàng
    @PostMapping("api/noauth/orders")
    public ResponseEntity<?> createOrderByCustomerId(@RequestBody COrders pOrder, @RequestParam Integer customerId) {
        return orderService.createOrderByCustomerId(pOrder, customerId);
    }

    // Lấy order qua customer id
    @GetMapping("orders/customer")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<?> getOrderByCustomerId(HttpServletRequest request) {
        return orderService.getOrderByCustomerId(request);
    }

    // Lấy order qua status
    @GetMapping("orders/status")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<?> getOrderByStatus(HttpServletRequest request, @RequestParam String status) {
        return orderService.getOrderByStatus(request, status);
    }

    // Huỷ đơn hàng
    @PutMapping("orders/cancel/{orderId}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<?> cancelOrderById(HttpServletRequest request, @PathVariable Integer orderId) {
        return orderService.cancelOrderById(request, orderId);
    }

    // ==================== MỤC DÀNH CHO QUẢN LÍ ============================
    // Cập nhật dữ liệu đơn hàng theo khách hàng
    @PutMapping("orders/{orderId}/customer/{customerId}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<?> updateOrderByCustomerId(@PathVariable Integer customerId,
            @PathVariable Integer orderId, @RequestBody COrders pOrder) {
        return orderService.updateOrderByCustomerId(customerId, orderId, pOrder);
    }

    // Lấy danh sách đơn hàng
    @GetMapping("orders")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<?> getAllOrders() {
        return orderService.getAllOrders();
    }

    // Lấy order của toàn bộ khách hàng qua status
    @GetMapping("orders/all/status")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<?> getAllOrderByStatus(@RequestParam String status) {
        return orderService.getAllOrderByStatus(status);
    }

    // Lấy toàn bộ đơn hành qua status và ngày tạo đơn
    @GetMapping("orders/filter")
    @PreAuthorize("hasRole('MANAGER')")
    public List<COrders> getAllOrderByStatusAndOrderDateRange(@RequestParam String status,
            @RequestParam String Date1, @RequestParam String Date2) {
        return orderService.getAllOrderByStatusAndOrderDateRange(status, Date1, Date2);
    }

    // Tìm khách hàng theo số lượng
    @GetMapping("orders/quantity")
    @PreAuthorize("hasRole('MANAGER')")
    List<?> findByQuantityOrder() {
        return ordersRepository.findByQuantityOrder();
    }
}

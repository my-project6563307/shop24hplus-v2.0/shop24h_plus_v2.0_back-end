package com.devcamp.shop24h.shop24hapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.CPayments;
import com.devcamp.shop24h.shop24hapi.repository.IPaymentsRepository;
import com.devcamp.shop24h.shop24hapi.service.CPaymentService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class CPaymentController {
    @Autowired
    private CPaymentService paymentService; 
    @Autowired
    private IPaymentsRepository paymentsRepository; 
    
    // ================ MỤC DÀNH CHO QUẢN TRỊ VIÊN (MANAGER ROLE) =====================


    //lấy toàn bộ danh sách hoá đơn của shop24h
    @GetMapping("payments")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<List<CPayments>> getAllPayments(@RequestParam(defaultValue = "0") String page,@RequestParam(defaultValue = "20") String size){
        return paymentService.getAllPayments(page, size);
    }

    // Lấy hoá đơn qua id hoá đơn
    @GetMapping("payments/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<CPayments> getPaymentById (@PathVariable Integer id){
        return paymentService.getPaymentById(id);
    }
    // Tạo hoá đơn cho khách hàng
    @PostMapping("payments")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> createPayment(@RequestBody CPayments pPayment,@RequestParam Integer customerId){
        return paymentService.createPayment(pPayment, customerId);
    }
    //Cập nhật hoá đơn cho khách hàng
    @PutMapping("payments/{paymentId}")
    @PreAuthorize("hasRole('MANAGER')")
    // Cập nhật hoá đơn cho khách hàng
    public ResponseEntity<Object> updatePayment(@PathVariable Integer paymentId,
    @RequestBody CPayments pPayment,@RequestParam Integer customerId){
        
        return paymentService.updatePayment(paymentId, pPayment, customerId);
    }

    // Lấy tổng tiền trong giai đoạn
    @GetMapping("payments/amount")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<?> getAnualAmount(@RequestParam String Date1,@RequestParam String Date2){
        return paymentService.getAnualAmount(Date1, Date2);
    }

    // Lấy danh sách hoá đơn 7 ngày gần nhất
    @GetMapping("payments/week")
    @PreAuthorize("hasRole('MANAGER')")
    public List<CPayments> getPayment7Days(){
        return paymentsRepository.getPayment7days();
    }

    // Lấy danh sách hoá đơn theo tháng
    @GetMapping("payments/month")
    @PreAuthorize("hasRole('MANAGER')")
    public List<?> getMonthProfit(){
        return paymentsRepository.getmonthProfit();
    }
    // Lấy danh sách hoá đơn theo tháng
    @GetMapping("payments/level")
    @PreAuthorize("hasRole('MANAGER')")
    List<?> getCustomerTotalPayment(){
        return paymentsRepository.getCustomerTotalPayment();
    }
}

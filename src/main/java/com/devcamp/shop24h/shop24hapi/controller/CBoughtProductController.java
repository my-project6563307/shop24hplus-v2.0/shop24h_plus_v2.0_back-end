package com.devcamp.shop24h.shop24hapi.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.CBoughtProducts;
import com.devcamp.shop24h.shop24hapi.service.CBoughtProductService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class CBoughtProductController {
    @Autowired
    private CBoughtProductService boughtProductService;

    // ======== CUSTOMER ============
    // Tạo viết review và đánh giá sản phẩm đã mua
    @PutMapping("feedbacks")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<Object> createFeedbackForBoughtProduct(HttpServletRequest request,
            @RequestParam Integer productId,
            @RequestBody CBoughtProducts feedback) {
        return boughtProductService.createFeedbackForBoughtProduct(request, productId, feedback);
    }

    @GetMapping("feedbacks")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    // Kiểm tra sản phẩm có được người dùng mua hay chưa?
    public ResponseEntity<Object> getFeedbackByCustomerIdAndProductId(HttpServletRequest request,
            @RequestParam Integer productId) {
        return boughtProductService.getFeedbackByCustomerIdAndProductId(request, productId);
    }

    @GetMapping("api/noauth/feedbacks")
    // Lấy danh sách đánh giá qua id sản phẩm
    public ResponseEntity<Object> getFeedbackByProductId(@RequestParam Integer productId) {
        return boughtProductService.getFeedbackByProductId(productId);
    }

    // ============== MANAGER =============
    // Lấy toàn bộ danh sách review
    @GetMapping("feedbacks/all")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> getAllFeedBack() {
        return boughtProductService.getAllFeedBack();
    }

    // xoá feedback với id
    @DeleteMapping("feedbacks/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> emptyFeedbackById(@PathVariable Integer id) {
        return boughtProductService.emptyFeedbackById(id);
    }
}

package com.devcamp.shop24h.shop24hapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "bought_products")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CBoughtProducts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String comments;
    
    private Integer rating;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "feedback_date")
    private Date feedbackDate;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CCustomers customer;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private CProducts product;
}

package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.shop24hapi.model.CPayments;

public interface IPaymentsRepository extends JpaRepository<CPayments, Integer>{
    List<CPayments> findByCustomerId(Integer customerId);

    @Query(value = "SELECT SUM(amount) FROM `payments` WHERE payment_date BETWEEN :Date1 AND :Date2",nativeQuery = true)
    Long findByRequest(@Param("Date1") String Date1,@Param("Date2") String Date2);

    @Query(value = "SELECT * FROM  payments WHERE payment_date BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()",nativeQuery = true)
    List<CPayments> getPayment7days();

    @Query(value = "SELECT MONTH(payment_date) AS month, SUM(amount) AS count FROM payments"
    + " WHERE YEAR(payment_date) = YEAR(CURDATE())"
    + " GROUP BY month;",nativeQuery = true)
    List<?> getmonthProfit();

    @Query(value = "SELECT SUM(amount), customer_id FROM `payments` GROUP BY customer_id ORDER BY SUM(amount) DESC",nativeQuery = true)
    List<?> getCustomerTotalPayment();
}

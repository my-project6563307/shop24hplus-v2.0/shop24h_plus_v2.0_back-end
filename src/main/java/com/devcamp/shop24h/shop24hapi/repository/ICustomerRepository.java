package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;

public interface ICustomerRepository extends JpaRepository<CCustomers, Integer>{
    
    Optional<CCustomers> findByUsername(String username);

    Optional<CCustomers> findByPhoneNumber(String phoneNumber);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Boolean existsByPhoneNumber(String phoneNumber);

    @Query(value = "SELECT * FROM `customers` WHERE total_payment BETWEEN ?1 AND ?2", nativeQuery = true)
    List<CCustomers> findByLevelOfPayment(@Param("payment1") String payment1,@Param("payment2") String payment2);
}

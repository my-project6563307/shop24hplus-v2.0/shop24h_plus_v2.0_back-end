package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.shop24hapi.model.CFavouriteProduct;

public interface IFavouriteProductRepository extends JpaRepository<CFavouriteProduct, Integer>{
    List<CFavouriteProduct> findByCustomerId(Integer customerId);
    CFavouriteProduct findByCustomerIdAndProductId(Integer customerId, Integer productId);
}

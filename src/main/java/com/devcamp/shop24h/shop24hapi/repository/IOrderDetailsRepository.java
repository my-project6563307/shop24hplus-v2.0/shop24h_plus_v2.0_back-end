package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.shop24hapi.model.COrderDetails;

public interface IOrderDetailsRepository extends JpaRepository<COrderDetails, Integer>{
    List<COrderDetails> findByOrderId(Integer orderId);
}

package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.shop24hapi.model.CBoughtProducts;

public interface IBoughtProductRepository extends JpaRepository<CBoughtProducts, Integer>{
    Optional<CBoughtProducts> findByCustomerIdAndProductId(Integer customerId, Integer productId);

    List<CBoughtProducts> findByProductId(Integer productId);
}

package com.devcamp.shop24h.shop24hapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.shop24hapi.model.CRoles;
import com.devcamp.shop24h.shop24hapi.model.ERole;

public interface IRolesRepository extends JpaRepository<CRoles, Long>{
    Optional<CRoles> findByName(ERole roleName);
}

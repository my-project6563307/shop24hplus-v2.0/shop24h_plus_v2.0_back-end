package com.devcamp.shop24h.shop24hapi.security.services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.repository.ICustomerRepository;
import com.devcamp.shop24h.shop24hapi.security.jwt.JwtUtils;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  ICustomerRepository cusCustomerRepository;

  @Autowired
  private JwtUtils jwtUtils;

  @Override
  @Transactional

  //Tìm user by username
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    CCustomers customer = cusCustomerRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

    return UserDetailsImpl.build(customer);
  }
  
  // Tìm customer by request login token
  public CCustomers whoami(HttpServletRequest request) {
    
    String token = resolveToken(request);

    String username = jwtUtils.getUserNameFromJwtToken(token);

    return cusCustomerRepository.findByUsername(username).get();
  }
  
  // Hàm xử lí chuỗi token
  private String resolveToken(HttpServletRequest req) {
    String bearerToken = req.getHeader("Authorization");
    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
      return bearerToken.substring(7);
    }
    return null;
  }
}



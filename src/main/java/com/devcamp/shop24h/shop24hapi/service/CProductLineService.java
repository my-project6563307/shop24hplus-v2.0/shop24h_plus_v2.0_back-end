package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CProductLines;
import com.devcamp.shop24h.shop24hapi.repository.IProductLinesRepository;

@Service
public class CProductLineService {
    @Autowired
    private IProductLinesRepository productLinesRepository;

    // ================ MỤC DÀNH CHO QUẢN TRỊ VIÊN(MANAGER) ===================

    // Lấy danh sách dòng sản phẩm
    public ResponseEntity<List<CProductLines>> getAllProductLines(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CProductLines> productLineList = new ArrayList<>();
            productLinesRepository.findAll(pageWithElements).forEach(productLineList::add);

            return new ResponseEntity<>(productLineList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy dòng sản phẩm qua id
    public ResponseEntity<CProductLines> getProductLineById (Integer id){
        try {
            Optional<CProductLines> productLineData = productLinesRepository.findById(id);
            if(productLineData.isPresent()){

                CProductLines productLineFound = productLineData.get();

                return new ResponseEntity<>(productLineFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Cập nhật các dòng sản phẩm qua id
    public ResponseEntity<Object> updateProductLineById(Integer id, CProductLines productLine){
        try {
            Optional<CProductLines> productLineData = productLinesRepository.findById(id);

            if (productLineData.isPresent()){

                CProductLines productLineFound =  productLineData.get();

                productLineFound.setProductLine(productLine.getProductLine());
                productLineFound.setDescription(productLine.getDescription());

                return new ResponseEntity<>(productLinesRepository.saveAndFlush(productLineFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Thêm mới dòng sản phẩm
    public ResponseEntity<Object> createProductLine(CProductLines productLine){
        try {
            
                CProductLines newProductLine =  new CProductLines();

                newProductLine.setProductLine(productLine.getProductLine());
                newProductLine.setDescription(productLine.getDescription());

                return new ResponseEntity<>(productLinesRepository.saveAndFlush(newProductLine), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá thông tin 1 dòng sản phẩm qua id
    public ResponseEntity<Object> deleteProductLineById(Integer id){
        try {
            Optional<CProductLines> productLineData = productLinesRepository.findById(id);

            if(productLineData.isPresent()){
                productLinesRepository.delete(productLineData.get());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

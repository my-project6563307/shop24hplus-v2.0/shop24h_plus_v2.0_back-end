package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.COffices;
import com.devcamp.shop24h.shop24hapi.repository.IOfficesRepository;

@Service
public class COfficeService {
    @Autowired
    private IOfficesRepository officesRepository;

    // ================ MỤC DÀNH CHO QUẢN TRỊ VIÊN (MANAGER ROLE) =====================

    //lấy toàn bộ danh sách văn phòng của shop24h
    public ResponseEntity<List<COffices>> getAllOffices(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<COffices> officeList = new ArrayList<>();
            officesRepository.findAll(pageWithElements).forEach(officeList::add);

            return new ResponseEntity<>(officeList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin văn phòng qua id văn phòng
    public ResponseEntity<COffices> getOfficeById (Integer id){
        try {
            Optional<COffices> officeData = officesRepository.findById(id);
            if(officeData.isPresent()){

                COffices officeFound = officeData.get();

                return new ResponseEntity<>(officeFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Cập nhật thông tin văn phòng qua id
    public ResponseEntity<Object> updateOfficeById(Integer id, COffices pOffice){
        try {
            Optional<COffices> officeData = officesRepository.findById(id);

            if (officeData.isPresent()){

                COffices officeFound =  officeData.get();

                officeFound.setCity(pOffice.getCity());
                officeFound.setPhone(pOffice.getPhone());
                officeFound.setAddress_line(pOffice.getAddress_line());
                officeFound.setCountry(pOffice.getCountry());
                officeFound.setState(pOffice.getState());
                officeFound.setTerritory(pOffice.getTerritory());

                return new ResponseEntity<>(officesRepository.saveAndFlush(officeFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Thêm mới văn phòng
    public ResponseEntity<Object> createOffice(COffices pOffice){
        try {
                COffices newOffice =  new COffices();

                newOffice.setCity(pOffice.getCity());
                newOffice.setPhone(pOffice.getPhone());
                newOffice.setAddress_line(pOffice.getAddress_line());
                newOffice.setCountry(pOffice.getCountry());
                newOffice.setState(pOffice.getState());
                newOffice.setTerritory(pOffice.getTerritory());

                return new ResponseEntity<>(officesRepository.saveAndFlush(newOffice), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Xoá thông tin 1 văn phòng
    public ResponseEntity<Object> deleteOfficeById(Integer id){
        try {
            Optional<COffices> officeData = officesRepository.findById(id);

            if(officeData.isPresent()){
                officesRepository.delete(officeData.get());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.shop24h.shop24hapi.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.COrderDetails;
import com.devcamp.shop24h.shop24hapi.model.COrders;
import com.devcamp.shop24h.shop24hapi.model.CProducts;
import com.devcamp.shop24h.shop24hapi.repository.IOrderDetailsRepository;
import com.devcamp.shop24h.shop24hapi.repository.IOrdersRepository;
import com.devcamp.shop24h.shop24hapi.repository.IProductsRepository;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsServiceImpl;

@Service
public class COrderDetailService {
    @Autowired
    private IOrderDetailsRepository orderDetailsRepository;

    @Autowired
    private IOrdersRepository ordersRepository;

    @Autowired
    private IProductsRepository productsRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;
    // ================== PUBLIC ===================== 

    // Tạo chi tiết đơn hàng( 1 hoặc nhiều sản phẩm) với id của đơn hàng đã tạo

    public ResponseEntity<?> createOrderDetails(Integer orderId, Integer productId, COrderDetails orderDetails){
        try {
            Optional<COrders> orderData = ordersRepository.findById(orderId);

            Optional<CProducts> productData = productsRepository.findById(productId);
            
            if(orderData.isPresent() && productData.isPresent()){

                COrderDetails newOrderDetail = new COrderDetails();
                CProducts productFound = productData.get();

                newOrderDetail.setOrder(orderData.get());
                newOrderDetail.setProduct(productFound);
                newOrderDetail.setPriceEach(productFound.getDiscountPrice());
                newOrderDetail.setQuantity_order(orderDetails.getQuantity_order());

                productFound.setQuantityInStock(productFound.getQuantityInStock() - orderDetails.getQuantity_order());

                if (productFound.getQuantityInStock() < 0){
                    return new ResponseEntity<>("insufficient quantity", HttpStatus.EXPECTATION_FAILED);
                }
                productsRepository.saveAndFlush(productFound);

                COrderDetails savedOrderDetail = orderDetailsRepository.saveAndFlush(newOrderDetail);

                return new ResponseEntity<>(savedOrderDetail, HttpStatus.CREATED);
            }

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy chi tiết đơn hàng qua order Id và của khách hàng cụ thể
    public ResponseEntity<?> getOrderDetailsByOrderId(HttpServletRequest request, Integer orderId){
        try {
            CCustomers customerData = userDetailsServiceImpl.whoami(request);
            
            Optional<COrders> orderData = ordersRepository.findById(orderId);

            if ( customerData.getId().equals(orderData.get().getCustomer().getId())){

                List<COrderDetails> listOrderDetails  = orderDetailsRepository.findByOrderId(orderId);

                return new ResponseEntity<>(listOrderDetails, HttpStatus.OK);
            }

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // cập nhật chi tiết đơn hàng qua order Id cho admin
    public ResponseEntity<?> updateOrderDetailsById(Integer orderDetailId, Integer productId, COrderDetails orderDetails){
        try {
            
            Optional<COrderDetails> orderDetailData = orderDetailsRepository.findById(orderDetailId);
            
            Optional<CProducts> productData = productsRepository.findById(productId);

            if(orderDetailData.isPresent()){

                CProducts productFound = productData.get();
                COrderDetails orderDetailUpdate = orderDetailData.get();

                productFound.setQuantityInStock(productFound.getQuantityInStock() - (orderDetails.getQuantity_order() - orderDetailUpdate.getQuantity_order()));

                
                if (productFound.getQuantityInStock() < 0){
                        return new ResponseEntity<>("insufficient quantity", HttpStatus.EXPECTATION_FAILED);
                    }

                orderDetailUpdate.setQuantity_order(orderDetails.getQuantity_order());
                productsRepository.saveAndFlush(productFound);

                return new ResponseEntity<>(orderDetailsRepository.saveAndFlush(orderDetailUpdate), HttpStatus.OK);
            }
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Lấy chi tiết đơn hàng qua order Id cho admin
    public ResponseEntity<?> getOrderDetailsByOrderId(Integer orderId){
        try {
            
            Optional<COrders> orderData = ordersRepository.findById(orderId);

            if(orderData.isPresent()){
                List<COrderDetails> listOrderDetails  = orderDetailsRepository.findByOrderId(orderId);
                return new ResponseEntity<>(listOrderDetails, HttpStatus.OK);
            }
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoá chi tiết đơn hàng qua order Id cho admin
    public ResponseEntity<?> deleteOrderDetailsByOrderDetailsId(Integer orderDetailsId, Integer productId){
        try {
            
            Optional<COrderDetails> orderDetailsData = orderDetailsRepository.findById(orderDetailsId);

            Optional<CProducts> productData = productsRepository.findById(productId);

            if(orderDetailsData.isPresent()){

                CProducts productFound = productData.get();
                
                productFound.setQuantityInStock(productFound.getQuantityInStock() + orderDetailsData.get().getQuantity_order());

                orderDetailsRepository.deleteById(orderDetailsId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

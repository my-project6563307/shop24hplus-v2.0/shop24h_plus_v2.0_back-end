package com.devcamp.shop24h.shop24hapi.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.CBoughtProducts;
import com.devcamp.shop24h.shop24hapi.model.COrderDetails;
import com.devcamp.shop24h.shop24hapi.model.COrders;
import com.devcamp.shop24h.shop24hapi.repository.ICustomerRepository;
import com.devcamp.shop24h.shop24hapi.repository.IBoughtProductRepository;
import com.devcamp.shop24h.shop24hapi.repository.IOrderDetailsRepository;
import com.devcamp.shop24h.shop24hapi.repository.IOrdersRepository;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsServiceImpl;

@Service
public class COrderService {
    @Autowired
    private IOrdersRepository ordersRepository;

    @Autowired
    private IOrderDetailsRepository orderDetailsRepository;

    @Autowired
    private ICustomerRepository customerRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private CBoughtProductService boughtProductService;

    @Autowired
    private IBoughtProductRepository boughtProductRepository;
    // ============ PUBLIC ==============================
    
    // Tạo order qua id khách hàng
    public ResponseEntity<?> createOrderByCustomerId(COrders pOrder, Integer customerId){
        try {
            Optional<CCustomers> customerData = customerRepository.findById(customerId);

            if (customerData.isPresent()){

                COrders newOrder = new COrders();

                newOrder.setCustomer(customerData.get());
                newOrder.setOrderDate(new Date());
                newOrder.setStatus("On hold");
                newOrder.setCreatePayment("no");
                newOrder.setDeliveryAddress(pOrder.getDeliveryAddress());
                newOrder.setComments(pOrder.getComments());

                COrders savedOrder = ordersRepository.saveAndFlush(newOrder);
                return new ResponseEntity<>(savedOrder, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy order qua id customer
    public ResponseEntity<?> getOrderByCustomerId(HttpServletRequest request){
        try {
            CCustomers customerData = userDetailsServiceImpl.whoami(request);
            
            List<COrders> orderList = ordersRepository.findByCustomerId(customerData.getId());
            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Lấy order qua status của mỗi khách hàng
    public ResponseEntity<?> getOrderByStatus(HttpServletRequest request, String status){
        try {
            CCustomers customerData = userDetailsServiceImpl.whoami(request);

            List<COrders> orderList = ordersRepository.findByStatusAndCustomerId(status,customerData.getId());

            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Huỷ order cho customer
    public ResponseEntity<?> cancelOrderById(HttpServletRequest request, Integer orderId){
        try {
            CCustomers customerData = userDetailsServiceImpl.whoami(request);

            Optional<COrders> orderData = ordersRepository.findByIdAndCustomerId(orderId, customerData.getId());

            if(orderData.isPresent()){
                COrders orderCancel = orderData.get();

                orderCancel.setStatus("Canceled");

                COrders savedOrder = ordersRepository.saveAndFlush(orderCancel);

                return new ResponseEntity<>(savedOrder, HttpStatus.OK);
            }

            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // ==================== MỤC DÀNH CHO QUẢN LÍ ============================
    // Lấy danh sách đơn hàng
    public ResponseEntity<?> getAllOrders(){
        try {
            List<COrders> orderList = ordersRepository.findAll();
            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Lấy order của toàn bộ qua status
    public ResponseEntity<?> getAllOrderByStatus(String status){
        try {

            List<COrders> orderList = ordersRepository.findByStatus(status);

            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //  Lấy toàn bộ đơn hành qua status và ngày tạo đơn
    public List<COrders> getAllOrderByStatusAndOrderDateRange(String status, String Date1, String Date2){

            if(status.equals("all")){
                status = "%";
            }
            return  ordersRepository.findByRequest(status, Date1, Date2);
    }
    // Cập nhật dữ liệu đơn hàng theo khách hàng
    public ResponseEntity<?> updateOrderByCustomerId (Integer customerId,Integer orderId, COrders pOrder){
        try {
            Optional<CCustomers> customerData = customerRepository.findById(customerId);

            Optional<COrders> orderData = ordersRepository.findById(orderId);

            if (orderData.isPresent() && customerData.isPresent()){

                COrders updateOrder = orderData.get();

                updateOrder.setCustomer(customerData.get());
                updateOrder.setOrderDate(pOrder.getOrderDate());
                updateOrder.setRequiredDate(pOrder.getRequiredDate());
                updateOrder.setShippedDate(pOrder.getShippedDate());
                updateOrder.setStatus(pOrder.getStatus());
                updateOrder.setComments(pOrder.getComments());
                updateOrder.setDeliveryAddress(pOrder.getDeliveryAddress());
                if(updateOrder.getCreatePayment().equals("yes")){
                    // ignore
                }
                else {
                    updateOrder.setCreatePayment(pOrder.getCreatePayment());
                }
                if(updateOrder.getStatus().equals("Shipped") == true){
                    List<COrderDetails> orderDetailsList = orderDetailsRepository.findByOrderId(orderId);
                    for(COrderDetails orderDetail:orderDetailsList){
                        // Kiểm tra xem sản phẩm đã được mua hay chưa
                        Optional<CBoughtProducts> boughtProductFound = boughtProductRepository.findByCustomerIdAndProductId(customerId, orderDetail.getProduct().getId());
                        if(boughtProductFound.isEmpty()){
                            // Chưa thì thêm sản phẩm vào danh sách
                            boughtProductService.createBoughtProductList(customerId, orderDetail.getProduct().getId());
                        }
                    }
                }
                COrders savedOrder = ordersRepository.saveAndFlush(updateOrder);
                return new ResponseEntity<>(savedOrder, HttpStatus.OK);
            }

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

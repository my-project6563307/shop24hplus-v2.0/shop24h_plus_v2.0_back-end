package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.CRoles;
import com.devcamp.shop24h.shop24hapi.model.ERole;
import com.devcamp.shop24h.shop24hapi.payload.request.LoginRequest;
import com.devcamp.shop24h.shop24hapi.payload.request.SignupRequest;
import com.devcamp.shop24h.shop24hapi.payload.response.JwtResponse;
import com.devcamp.shop24h.shop24hapi.payload.response.MessageResponse;
import com.devcamp.shop24h.shop24hapi.repository.ICustomerRepository;
import com.devcamp.shop24h.shop24hapi.repository.IRolesRepository;
import com.devcamp.shop24h.shop24hapi.security.jwt.JwtUtils;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsImpl;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsServiceImpl;

@Service
public class CCustomerService {
    @Autowired
    private ICustomerRepository customerRepository;

    @Autowired
    private PasswordEncoder encoder;
  
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private IRolesRepository rolesRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    // ================== PUBLIC =====================

    // Kiểm tra khả dụng đăng nhập người dùng
    public ResponseEntity<?> checkCustomerAvailableByRequest(HttpServletRequest request) {
        try {
            CCustomers customerChecked = userDetailsServiceImpl.whoami(request);
            return new ResponseEntity<CCustomers>(customerChecked, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Kiểm tra thông tin khách hàng qua phone number/ có thì gửi thông tin khách hàng về
    //  Không có thì tạo mới khách hàng và trả thông tin KH về
    public ResponseEntity<?> checkCustomer(CCustomers pCustomer){
        try {
            //Tìm người dùng
            Optional<CCustomers> customerData = customerRepository.findByPhoneNumber(pCustomer.getPhoneNumber());

            // Cài đặt người dùng mới luôn có role là customer
            Optional<CRoles> userRole = rolesRepository.findByName(ERole.ROLE_CUSTOMER);

            Set<CRoles> roles = new HashSet<>();

            roles.add(userRole.get());

            if (customerData.isPresent()){
                return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
            }
            else {
                CCustomers newCustomer =  new CCustomers();
                newCustomer.setRoles(roles);
                newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
                newCustomer.setFirstName(pCustomer.getFirstName());
                newCustomer.setLastName(pCustomer.getLastName());
                newCustomer.setAddress(pCustomer.getAddress());

                CCustomers  savedCustomer = customerRepository.saveAndFlush(newCustomer);

            return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // ================== LOGIN AND REGISTER ===========================

    // Đăng nhập vào hệ thống nhận mã jwt.
    public ResponseEntity<?> authenticateUser(LoginRequest loginRequest) {
        Optional<CCustomers> customerFound = customerRepository.findByUsername(loginRequest.getUsername());
        if(customerFound.isPresent() && customerFound.get().isActive() == true){
            Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);
            
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();    
            List<String> roles = userDetails.getAuthorities().stream()
                                .map(item -> item.getAuthority())
                                .collect(Collectors.toList());
    
            return ResponseEntity.ok(new JwtResponse(jwt,
                                "Bear", 
                                userDetails.getId(), 
                                userDetails.getUsername(), 
                                userDetails.getEmail(), 
                                roles));
        }
        return new ResponseEntity<>("This account is not available", HttpStatus.NOT_FOUND);
    }
  
    // Đăng kí người dùng mới
    public ResponseEntity<?> registerUser(SignupRequest signUpRequest) {
        // Kiểm tra người dùng có tồn tại hay chưa
        if (customerRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (customerRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Email is already in use!"));
        }
        // Tạo thông tin người dùng
        CCustomers customer = new CCustomers();
        customer.setUsername(signUpRequest.getUsername());
        customer.setEmail(signUpRequest.getEmail());
        customer.setPassword(encoder.encode(signUpRequest.getPassword()));

        // Người dùng mới luôn có role là customer
        Optional<CRoles> userRole = rolesRepository.findByName(ERole.ROLE_CUSTOMER);

        Set<CRoles> roles = new HashSet<>();

        roles.add(userRole.get());

        customer.setRoles(roles);

        // Lưu vào kho người dùng
        customerRepository.saveAndFlush(customer);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

     //Lọc khách hàng qua tổng tiền đơn hàng đã mua
     public ResponseEntity<Object> findByLevelOfPayment (String payment1, String payment2){
        try {
            List<CCustomers> customerList = customerRepository.findByLevelOfPayment(payment1, payment2);

            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }
    // ============================= MỤC DÀNH CHO QUYỀN MANAGER ===================================

    //lấy toàn bộ danh sách khách hàng
    public ResponseEntity<List<CCustomers>> getAllCustomers(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CCustomers> customerList = new ArrayList<>();
            customerRepository.findAll(pageWithElements).forEach(customerList::add);

            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Lấy thông tin khách hàng qua id;
    public ResponseEntity<CCustomers> getCustomerById (Integer id){
        try {
            Optional<CCustomers> customerData = customerRepository.findById(id);
            if(customerData.isPresent()){

                CCustomers customerFound = customerData.get();

                return new ResponseEntity<>(customerFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Cập nhật thông tin khách hàng qua id
    public ResponseEntity<Object> updateCustomerById(Integer id, CCustomers pCustomer,
            Long roleId){
        try {
            Optional<CCustomers> customerData = customerRepository.findById(id);

            Optional<CRoles> roleData = rolesRepository.findById(roleId);
            Set<CRoles> roleSet = new HashSet<>();
            roleSet.add(roleData.get());
            
            if (customerData.isPresent()){

                CCustomers customerFound =  customerData.get();

                customerFound.setRoles(roleSet);
                customerFound.setUsername(pCustomer.getUsername());
                customerFound.setFirstName(pCustomer.getFirstName());
                customerFound.setLastName(pCustomer.getLastName());
                customerFound.setPhoneNumber(pCustomer.getPhoneNumber());
                customerFound.setEmail(pCustomer.getEmail());
                customerFound.setPhoto(pCustomer.getPhoto());
                customerFound.setAddress(pCustomer.getAddress());
                customerFound.setCity(pCustomer.getCity());
                customerFound.setState(pCustomer.getState());
                customerFound.setPostalCode(pCustomer.getPostalCode());
                customerFound.setCountry(pCustomer.getCountry());
                customerFound.setCreditLimit(pCustomer.getCreditLimit());
                customerFound.setActive(pCustomer.isActive());
                if(encoder.matches(pCustomer.getPassword(),customerFound.getPassword())){

                }
                else if(pCustomer.getPassword().equals(customerFound.getPassword())){
                    
                }
                else {
                    customerFound.setPassword(encoder.encode(pCustomer.getPassword()));
                }
                return new ResponseEntity<>(customerRepository.saveAndFlush(customerFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Thêm mới khách hàng
    public ResponseEntity<Object> createCustomer(CCustomers pCustomer,
            Long roleId){
        try {
            // Tạo role cho khách hàng
            Optional<CRoles> roleData = rolesRepository.findById(roleId);
            Set<CRoles> roleSet = new HashSet<>();
            roleSet.add(roleData.get());

            // Kiểm tra người dùng có tồn tại hay chưa
        if (customerRepository.existsByUsername(pCustomer.getUsername())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (customerRepository.existsByEmail(pCustomer.getEmail())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Email is already in use!"));
        }

        if (customerRepository.existsByPhoneNumber(pCustomer.getPhoneNumber())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Phone number is already in use!"));
        }
        // Tạo mới người dùng
            CCustomers newCustomer =  new CCustomers();

            newCustomer.setRoles(roleSet);
            newCustomer.setUsername(pCustomer.getUsername());
            newCustomer.setPassword(encoder.encode(pCustomer.getPassword()));
            newCustomer.setFirstName(pCustomer.getFirstName());
            newCustomer.setLastName(pCustomer.getLastName());
            newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
            newCustomer.setEmail(pCustomer.getEmail());
            newCustomer.setPhoto(pCustomer.getPhoto());
            newCustomer.setAddress(pCustomer.getAddress());
            newCustomer.setCity(pCustomer.getCity());
            newCustomer.setState(pCustomer.getState());
            newCustomer.setPostalCode(pCustomer.getPostalCode());
            newCustomer.setCountry(pCustomer.getCountry());
            newCustomer.setCreditLimit(pCustomer.getCreditLimit());
            newCustomer.setActive(pCustomer.isActive());

            return new ResponseEntity<>(customerRepository.saveAndFlush(newCustomer), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete Khách hàng by id
    public ResponseEntity<Object> deleteCustomerById(Integer id){
        try {
            Optional<CCustomers> customerData = customerRepository.findById(id);

            if(customerData.isPresent()){
                customerRepository.delete(customerData.get());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // =======================================================================================
    // ===================== MỤC DÀNH CHO KHÁCH HÀNG (CUSTOMER ROLE)==========================

    //Cập nhật người dùng qua mã token trong request
    public ResponseEntity<?> updateCustomerByRequest(HttpServletRequest request, CCustomers pCustomer ){

        try {
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);
            // Kiểm tra người dùng có tồn tại email hay sdt chưa
            if (customerRepository.existsByEmail(pCustomer.getEmail())) {
                if(customerFound.getEmail().equals(pCustomer.getEmail()) == false){
    
                    return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Error: Email is already in use!"));
                }
            }
            if (customerRepository.existsByPhoneNumber(pCustomer.getPhoneNumber())) {
                if(customerFound.getPhoneNumber().equals(pCustomer.getPhoneNumber()) == false){
                    
                    return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Phone number is already in use!"));
                }
            }

            customerFound.setFirstName(pCustomer.getFirstName());
            customerFound.setLastName(pCustomer.getLastName());
            customerFound.setPhoneNumber(pCustomer.getPhoneNumber());
            customerFound.setEmail(pCustomer.getEmail());
            customerFound.setPhoto(pCustomer.getPhoto());
            customerFound.setAddress(pCustomer.getAddress());
            customerFound.setCity(pCustomer.getCity());
            customerFound.setState(pCustomer.getState());
            customerFound.setPostalCode(pCustomer.getPostalCode());
            customerFound.setCountry(pCustomer.getCountry());
            customerFound.setCreditLimit(pCustomer.getCreditLimit());

            customerRepository.saveAndFlush(customerFound);

            return new ResponseEntity<>(customerRepository.saveAndFlush(customerFound), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Cập nhật mật khẩu người dùng
    public ResponseEntity<String> updateCustomerPasswordByRequest(HttpServletRequest request,
    String oldPassword, String newPassword){
        
        try {
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);

            if(encoder.matches(oldPassword, customerFound.getPassword())){
                customerFound.setPassword(encoder.encode(newPassword));
                customerRepository.saveAndFlush(customerFound);
                return new ResponseEntity<>("Change password successfully!", HttpStatus.OK);
            }
            return new ResponseEntity<>("Your old password is wrong!", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

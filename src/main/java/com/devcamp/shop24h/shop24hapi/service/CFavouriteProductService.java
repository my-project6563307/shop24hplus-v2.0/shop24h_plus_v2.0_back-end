package com.devcamp.shop24h.shop24hapi.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.CFavouriteProduct;
import com.devcamp.shop24h.shop24hapi.model.CProducts;
import com.devcamp.shop24h.shop24hapi.repository.IFavouriteProductRepository;
import com.devcamp.shop24h.shop24hapi.repository.IProductsRepository;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsServiceImpl;

@Service
public class CFavouriteProductService {
    @Autowired
    private IFavouriteProductRepository favouriteProductRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private IProductsRepository productsRepository;
    //================ CUSTOMER=================
    // Lấy danh sách sản phẩm ưa thích theo khách hàng
    public ResponseEntity<Object> getFavouriteProduct(HttpServletRequest request){
        try {
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);
            List<CFavouriteProduct> listFavouriteProduct = favouriteProductRepository.findByCustomerId(customerFound.getId());
            if(listFavouriteProduct.size() > 0){
                return new ResponseEntity<>(listFavouriteProduct, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Tạo sản phẩm ưa thích theo khách hàng và sản phẩm
    public ResponseEntity<Object> createFavouriteProduct(HttpServletRequest request, Integer productId){
        try {
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);

            Optional<CProducts> productFound = productsRepository.findById(productId);

            if(productFound.isPresent()){
                CFavouriteProduct newFavouriteProduct = new CFavouriteProduct();
                newFavouriteProduct.setCustomer(customerFound);
                newFavouriteProduct.setProduct(productFound.get());

                return new ResponseEntity<>(favouriteProductRepository.saveAndFlush(newFavouriteProduct), HttpStatus.OK);
            }

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá sản phẩm yêu thích theo id
    public ResponseEntity<Object> deleteFavouriteProduct(HttpServletRequest request, Integer productId){

        try {
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);

            CFavouriteProduct favouriteProductFound = favouriteProductRepository.findByCustomerIdAndProductId(customerFound.getId(), productId);

            favouriteProductRepository.deleteById(favouriteProductFound.getId());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   

}

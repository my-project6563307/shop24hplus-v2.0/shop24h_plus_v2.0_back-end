package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.CBoughtProducts;
import com.devcamp.shop24h.shop24hapi.model.CProducts;
import com.devcamp.shop24h.shop24hapi.payload.response.MessageResponse;
import com.devcamp.shop24h.shop24hapi.repository.ICustomerRepository;
import com.devcamp.shop24h.shop24hapi.repository.IBoughtProductRepository;
import com.devcamp.shop24h.shop24hapi.repository.IProductsRepository;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsServiceImpl;

@Service
public class CBoughtProductService {
    @Autowired
    private IBoughtProductRepository boughtProductRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private IProductsRepository productsRepository;

    @Autowired
    private ICustomerRepository customerRepository;
    // =============CUSTOMER==============

    // Tạo danh sách sản phẩm được mua
    public ResponseEntity<Object> createBoughtProductList(Integer customerId, Integer productId){
        try {
            // Sản phẩm cần đánh giá
            Optional<CProducts> productData = productsRepository.findById(productId);
            Optional<CCustomers> customerData = customerRepository.findById(customerId);
           
            CBoughtProducts newBoughtProduct = new CBoughtProducts();

            newBoughtProduct.setCustomer(customerData.get());
            newBoughtProduct.setProduct(productData.get());

            return new ResponseEntity<>(boughtProductRepository.saveAndFlush(newBoughtProduct), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Cập nhật review và đánh giá sản phẩm đã mua
    public ResponseEntity<Object> createFeedbackForBoughtProduct(HttpServletRequest request, Integer productId, CBoughtProducts feedback){
        try {
            // Sản phẩm cần đánh giá của khách hàng đã mua
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);
            
            // Tìm sản phẩm trong danh sách đã mua
            Optional<CBoughtProducts> boughtProductData = 
            boughtProductRepository.findByCustomerIdAndProductId(customerFound.getId(), productId);

            if(boughtProductData.isPresent()){

                CBoughtProducts newFeedback = boughtProductData.get();
                newFeedback.setComments(feedback.getComments());
                newFeedback.setRating(feedback.getRating());
                newFeedback.setFeedbackDate(new Date());
    
                return new ResponseEntity<>(boughtProductRepository.saveAndFlush(newFeedback), HttpStatus.CREATED);
            }

            return new ResponseEntity<>(new MessageResponse("You need buy this product to leave a feedback"), HttpStatus.EXPECTATION_FAILED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Kiểm tra sản phẩm có được người dùng mua hay chưa?
    public ResponseEntity<Object> getFeedbackByCustomerIdAndProductId(HttpServletRequest request, Integer productId){
        try {
            // Sản phẩm cần đánh giá của khách hàng đã mua
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);
            
            // Tìm sản phẩm trong danh sách đã mua
            Optional<CBoughtProducts> boughtProductData = 
            boughtProductRepository.findByCustomerIdAndProductId(customerFound.getId(), productId);

            if(boughtProductData.isPresent()){
                CBoughtProducts boughtProductFound = boughtProductData.get();
                return new ResponseEntity<>(boughtProductFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy danh sách đánh giá qua id sản phẩm
    public ResponseEntity<Object> getFeedbackByProductId(Integer productId){
        try {
            List<CBoughtProducts> feedbackList = boughtProductRepository.findByProductId(productId);
            List<CBoughtProducts> feedbackFilter = new ArrayList<>();
            if(feedbackList.size() > 0) {
                for(CBoughtProducts boughtProducts:feedbackList){
                    if(boughtProducts.getRating() != null){
                        feedbackFilter.add(boughtProducts);
                    }
                }
                return new ResponseEntity<>(feedbackFilter, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // ============== MANAGER =============
    // Lấy toàn bộ danh sách review
    public ResponseEntity<Object> getAllFeedBack(){
        try {
            List<CBoughtProducts> boughtProductsList = boughtProductRepository.findAll();
            List<CBoughtProducts> feedbackFilter = new ArrayList<>();

            if(boughtProductsList.size() > 0) {
                for(CBoughtProducts boughtProducts:boughtProductsList){
                    if(boughtProducts.getRating() != null){
                        feedbackFilter.add(boughtProducts);
                    }
                }
                return new ResponseEntity<>(feedbackFilter, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // xoá feedback với id
    public ResponseEntity<Object> emptyFeedbackById(Integer id){
        try {
            Optional<CBoughtProducts> boughtProductData = boughtProductRepository.findById(id);

            if(boughtProductData.isPresent()) {
                CBoughtProducts emptyFeedback = boughtProductData.get();

                emptyFeedback.setComments(null);
                emptyFeedback.setRating(null);
                emptyFeedback.setFeedbackDate(null);
                boughtProductRepository.saveAndFlush(emptyFeedback);
                
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
